const express = require('express')
const app = express()
const port = 80

app.use(express.json())

app.post('/', (req, res) => {
  console.log(req.body);
  const data = req.body;

  if (data.session.new) return res.json({
    response: {
      text: "Выберите животное: корова, свинья, петух",
      tts: "Здравствуйте, выберите то животное, чей звук вы ходите услышать.",
      end_session: false
    },
    session: {
      session_id: data.session.session_id,
      message_id: data.session.message_id,
      user_id: data.session.user_id
    },
    version: "1.0"
  })

  if(data.request.command === 'on_interrupt') {
    return res.json({
      response: {
        text: "Пока, лох",
        end_session: true
      },
      session: {
        session_id: data.session.session_id,
        message_id: data.session.message_id,
        user_id: data.session.user_id
      },
      version: "1.0"
    })
  }

  switch (data.request.original_utterance) {
    case 'петух':
      return res.json({
        response: {
          text: "Воспроизвожу звук петуха",
          tts: "Кукарекуууууууу нахуй",
          end_session: false
        },
        session: {
          session_id: data.session.session_id,
          message_id: data.session.message_id,
          user_id: data.session.user_id
        },
        version: "1.0"
      })
    case 'корова':
      return res.json({
        response: {
          text: "Воспроизвожу мычание коровы",
          tts: "Мууууууууу. дак.",
          end_session: false
        },
        session: {
          session_id: data.session.session_id,
          message_id: data.session.message_id,
          user_id: data.session.user_id
        },
        version: "1.0"
      })
    case 'свинья':
      return res.json({
        response: {
          text: "Воспроизвожу хрюканье свиньи",
          tts: "Хрю хрю ёпта",
          end_session: false
        },
        session: {
          session_id: data.session.session_id,
          message_id: data.session.message_id,
          user_id: data.session.user_id
        },
        version: "1.0"
      })
    default:
      return res.json({
        response: {
          text: "Я не поняла вас",
          end_session: false
        },
        session: {
          session_id: data.session.session_id,
          message_id: data.session.message_id,
          user_id: data.session.user_id
        },
        version: "1.0"
      })
  }

})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})